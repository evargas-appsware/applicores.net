﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LiicoresApp_Admin.Startup))]
namespace LiicoresApp_Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
