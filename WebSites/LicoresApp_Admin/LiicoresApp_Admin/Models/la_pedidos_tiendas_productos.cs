//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LiicoresApp_Admin.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class la_pedidos_tiendas_productos
    {
        public int id { get; set; }
        public Nullable<int> id_tiendas_productos { get; set; }
        public Nullable<int> cantidad { get; set; }
        public Nullable<int> precio_final { get; set; }
        public Nullable<int> id_pedidos { get; set; }
    
        public virtual la_pedidos la_pedidos { get; set; }
        public virtual la_tiendas_productos la_tiendas_productos { get; set; }
    }
}
