﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LiicoresApp_Admin.Models;

namespace LiicoresApp_Admin.Controllers
{
    public class PedidosTiendasProductosController : Controller
    {
        private LicoresAppEntities db = new LicoresAppEntities();

        // GET: PedidosTiendasProductos
        public ActionResult Index()
        {
            var la_pedidos_tiendas_productos = db.la_pedidos_tiendas_productos.Include(l => l.la_pedidos).Include(l => l.la_tiendas_productos);
            return View(la_pedidos_tiendas_productos.ToList());
        }

        // GET: PedidosTiendasProductos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_pedidos_tiendas_productos la_pedidos_tiendas_productos = db.la_pedidos_tiendas_productos.Find(id);
            if (la_pedidos_tiendas_productos == null)
            {
                return HttpNotFound();
            }
            return View(la_pedidos_tiendas_productos);
        }

        // GET: PedidosTiendasProductos/Create
        public ActionResult Create()
        {
            ViewBag.id_pedidos = new SelectList(db.la_pedidos, "id", "estado");
            ViewBag.id_tiendas_productos = new SelectList(db.la_tiendas_productos, "id", "id");
            return View();
        }

        // POST: PedidosTiendasProductos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_tiendas_productos,cantidad,precio_final,id_pedidos")] la_pedidos_tiendas_productos la_pedidos_tiendas_productos)
        {
            if (ModelState.IsValid)
            {
                db.la_pedidos_tiendas_productos.Add(la_pedidos_tiendas_productos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_pedidos = new SelectList(db.la_pedidos, "id", "estado", la_pedidos_tiendas_productos.id_pedidos);
            ViewBag.id_tiendas_productos = new SelectList(db.la_tiendas_productos, "id", "id", la_pedidos_tiendas_productos.id_tiendas_productos);
            return View(la_pedidos_tiendas_productos);
        }

        // GET: PedidosTiendasProductos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_pedidos_tiendas_productos la_pedidos_tiendas_productos = db.la_pedidos_tiendas_productos.Find(id);
            if (la_pedidos_tiendas_productos == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_pedidos = new SelectList(db.la_pedidos, "id", "estado", la_pedidos_tiendas_productos.id_pedidos);
            ViewBag.id_tiendas_productos = new SelectList(db.la_tiendas_productos, "id", "id", la_pedidos_tiendas_productos.id_tiendas_productos);
            return View(la_pedidos_tiendas_productos);
        }

        // POST: PedidosTiendasProductos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_tiendas_productos,cantidad,precio_final,id_pedidos")] la_pedidos_tiendas_productos la_pedidos_tiendas_productos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(la_pedidos_tiendas_productos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_pedidos = new SelectList(db.la_pedidos, "id", "estado", la_pedidos_tiendas_productos.id_pedidos);
            ViewBag.id_tiendas_productos = new SelectList(db.la_tiendas_productos, "id", "id", la_pedidos_tiendas_productos.id_tiendas_productos);
            return View(la_pedidos_tiendas_productos);
        }

        // GET: PedidosTiendasProductos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_pedidos_tiendas_productos la_pedidos_tiendas_productos = db.la_pedidos_tiendas_productos.Find(id);
            if (la_pedidos_tiendas_productos == null)
            {
                return HttpNotFound();
            }
            return View(la_pedidos_tiendas_productos);
        }

        // POST: PedidosTiendasProductos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            la_pedidos_tiendas_productos la_pedidos_tiendas_productos = db.la_pedidos_tiendas_productos.Find(id);
            db.la_pedidos_tiendas_productos.Remove(la_pedidos_tiendas_productos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
