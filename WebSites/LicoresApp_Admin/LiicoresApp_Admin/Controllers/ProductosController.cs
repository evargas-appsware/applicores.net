﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LiicoresApp_Admin.Models;

namespace LiicoresApp_Admin.Controllers
{
    public class ProductosController : Controller
    {
        private LicoresAppEntities db = new LicoresAppEntities();

        // GET: Productos
        public ActionResult Index()
        {
            var la_productos = db.la_productos.Include(l => l.la_categorias);
            return View(la_productos.ToList());
        }

        // GET: Productos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_productos la_productos = db.la_productos.Find(id);
            if (la_productos == null)
            {
                return HttpNotFound();
            }
            return View(la_productos);
        }

        // GET: Productos/Create
        public ActionResult Create()
        {
            ViewBag.id_categoria = new SelectList(db.la_categorias, "id", "nombre");
            return View();
        }

        // POST: Productos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,nombre,descripcion,precio,imagen,id_categoria")] la_productos la_productos)
        {
            if (ModelState.IsValid)
            {
                db.la_productos.Add(la_productos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_categoria = new SelectList(db.la_categorias, "id", "nombre", la_productos.id_categoria);
            return View(la_productos);
        }

        // GET: Productos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_productos la_productos = db.la_productos.Find(id);
            if (la_productos == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_categoria = new SelectList(db.la_categorias, "id", "nombre", la_productos.id_categoria);
            return View(la_productos);
        }

        // POST: Productos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,nombre,descripcion,precio,imagen,id_categoria")] la_productos la_productos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(la_productos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_categoria = new SelectList(db.la_categorias, "id", "nombre", la_productos.id_categoria);
            return View(la_productos);
        }

        // GET: Productos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_productos la_productos = db.la_productos.Find(id);
            if (la_productos == null)
            {
                return HttpNotFound();
            }
            return View(la_productos);
        }

        // POST: Productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            la_productos la_productos = db.la_productos.Find(id);
            db.la_productos.Remove(la_productos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
