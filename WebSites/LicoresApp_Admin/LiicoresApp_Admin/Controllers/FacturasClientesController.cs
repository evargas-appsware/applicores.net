﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LiicoresApp_Admin.Models;

namespace LiicoresApp_Admin.Controllers
{
    public class FacturasClientesController : Controller
    {
        private LicoresAppEntities db = new LicoresAppEntities();

        // GET: FacturasClientes
        public ActionResult Index()
        {
            var la_facturas_clientes = db.la_facturas_clientes.Include(l => l.la_clientes);
            return View(la_facturas_clientes.ToList());
        }

        // GET: FacturasClientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_facturas_clientes la_facturas_clientes = db.la_facturas_clientes.Find(id);
            if (la_facturas_clientes == null)
            {
                return HttpNotFound();
            }
            return View(la_facturas_clientes);
        }

        // GET: FacturasClientes/Create
        public ActionResult Create()
        {
            ViewBag.id_cliente = new SelectList(db.la_clientes, "id_rut", "nombre");
            return View();
        }

        // POST: FacturasClientes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_cliente,fecha_emision,fecha_vencimiento,deuda,estado")] la_facturas_clientes la_facturas_clientes)
        {
            if (ModelState.IsValid)
            {
                db.la_facturas_clientes.Add(la_facturas_clientes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_cliente = new SelectList(db.la_clientes, "id_rut", "nombre", la_facturas_clientes.id_cliente);
            return View(la_facturas_clientes);
        }

        // GET: FacturasClientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_facturas_clientes la_facturas_clientes = db.la_facturas_clientes.Find(id);
            if (la_facturas_clientes == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_cliente = new SelectList(db.la_clientes, "id_rut", "nombre", la_facturas_clientes.id_cliente);
            return View(la_facturas_clientes);
        }

        // POST: FacturasClientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_cliente,fecha_emision,fecha_vencimiento,deuda,estado")] la_facturas_clientes la_facturas_clientes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(la_facturas_clientes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_cliente = new SelectList(db.la_clientes, "id_rut", "nombre", la_facturas_clientes.id_cliente);
            return View(la_facturas_clientes);
        }

        // GET: FacturasClientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_facturas_clientes la_facturas_clientes = db.la_facturas_clientes.Find(id);
            if (la_facturas_clientes == null)
            {
                return HttpNotFound();
            }
            return View(la_facturas_clientes);
        }

        // POST: FacturasClientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            la_facturas_clientes la_facturas_clientes = db.la_facturas_clientes.Find(id);
            db.la_facturas_clientes.Remove(la_facturas_clientes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
