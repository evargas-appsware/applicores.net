﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LiicoresApp_Admin.Models;

namespace LiicoresApp_Admin.Controllers
{
    public class TiendasController : Controller
    {
        private LicoresAppEntities db = new LicoresAppEntities();

        // GET: Tiendas
        public ActionResult Index()
        {
            var la_tiendas = db.la_tiendas.Include(l => l.la_clientes);
            return View(la_tiendas.ToList());
        }

        // GET: Tiendas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_tiendas la_tiendas = db.la_tiendas.Find(id);
            if (la_tiendas == null)
            {
                return HttpNotFound();
            }
            return View(la_tiendas);
        }

        // GET: Tiendas/Create
        public ActionResult Create()
        {
            ViewBag.id_cliente = new SelectList(db.la_clientes, "id_rut", "nombre");
            return View();
        }

        // POST: Tiendas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,nombre,descripcion,direccion,imagen,id_cliente")] la_tiendas la_tiendas)
        {
            if (ModelState.IsValid)
            {
                db.la_tiendas.Add(la_tiendas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_cliente = new SelectList(db.la_clientes, "id_rut", "nombre", la_tiendas.id_cliente);
            return View(la_tiendas);
        }

        // GET: Tiendas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_tiendas la_tiendas = db.la_tiendas.Find(id);
            if (la_tiendas == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_cliente = new SelectList(db.la_clientes, "id_rut", "nombre", la_tiendas.id_cliente);
            return View(la_tiendas);
        }

        // POST: Tiendas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,nombre,descripcion,direccion,imagen,id_cliente")] la_tiendas la_tiendas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(la_tiendas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_cliente = new SelectList(db.la_clientes, "id_rut", "nombre", la_tiendas.id_cliente);
            return View(la_tiendas);
        }

        // GET: Tiendas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_tiendas la_tiendas = db.la_tiendas.Find(id);
            if (la_tiendas == null)
            {
                return HttpNotFound();
            }
            return View(la_tiendas);
        }

        // POST: Tiendas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            la_tiendas la_tiendas = db.la_tiendas.Find(id);
            db.la_tiendas.Remove(la_tiendas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
