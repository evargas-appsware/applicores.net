﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LiicoresApp_Admin.Models;

namespace LiicoresApp_Admin.Controllers
{
    public class TiendasProductosController : Controller
    {
        private LicoresAppEntities db = new LicoresAppEntities();

        // GET: TiendasProductos
        public ActionResult Index()
        {
            var la_tiendas_productos = db.la_tiendas_productos.Include(l => l.la_productos).Include(l => l.la_tiendas);
            return View(la_tiendas_productos.ToList());
        }

        // GET: TiendasProductos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_tiendas_productos la_tiendas_productos = db.la_tiendas_productos.Find(id);
            if (la_tiendas_productos == null)
            {
                return HttpNotFound();
            }
            return View(la_tiendas_productos);
        }

        // GET: TiendasProductos/Create
        public ActionResult Create()
        {
            ViewBag.id_producto = new SelectList(db.la_productos, "id", "nombre");
            ViewBag.id_tienda = new SelectList(db.la_tiendas, "id", "nombre");
            return View();
        }

        // POST: TiendasProductos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_tienda,id_producto,stock_producto")] la_tiendas_productos la_tiendas_productos)
        {
            if (ModelState.IsValid)
            {
                db.la_tiendas_productos.Add(la_tiendas_productos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_producto = new SelectList(db.la_productos, "id", "nombre", la_tiendas_productos.id_producto);
            ViewBag.id_tienda = new SelectList(db.la_tiendas, "id", "nombre", la_tiendas_productos.id_tienda);
            return View(la_tiendas_productos);
        }

        // GET: TiendasProductos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_tiendas_productos la_tiendas_productos = db.la_tiendas_productos.Find(id);
            if (la_tiendas_productos == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_producto = new SelectList(db.la_productos, "id", "nombre", la_tiendas_productos.id_producto);
            ViewBag.id_tienda = new SelectList(db.la_tiendas, "id", "nombre", la_tiendas_productos.id_tienda);
            return View(la_tiendas_productos);
        }

        // POST: TiendasProductos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_tienda,id_producto,stock_producto")] la_tiendas_productos la_tiendas_productos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(la_tiendas_productos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_producto = new SelectList(db.la_productos, "id", "nombre", la_tiendas_productos.id_producto);
            ViewBag.id_tienda = new SelectList(db.la_tiendas, "id", "nombre", la_tiendas_productos.id_tienda);
            return View(la_tiendas_productos);
        }

        // GET: TiendasProductos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_tiendas_productos la_tiendas_productos = db.la_tiendas_productos.Find(id);
            if (la_tiendas_productos == null)
            {
                return HttpNotFound();
            }
            return View(la_tiendas_productos);
        }

        // POST: TiendasProductos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            la_tiendas_productos la_tiendas_productos = db.la_tiendas_productos.Find(id);
            db.la_tiendas_productos.Remove(la_tiendas_productos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
