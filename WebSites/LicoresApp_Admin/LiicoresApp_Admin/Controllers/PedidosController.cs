﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LiicoresApp_Admin.Models;

namespace LiicoresApp_Admin.Controllers
{
    public class PedidosController : Controller
    {
        private LicoresAppEntities db = new LicoresAppEntities();

        // GET: Pedidos
        public ActionResult Index()
        {
            var la_pedidos = db.la_pedidos.Include(l => l.la_usuarios);
            return View(la_pedidos.ToList());
        }

        // GET: Pedidos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_pedidos la_pedidos = db.la_pedidos.Find(id);
            if (la_pedidos == null)
            {
                return HttpNotFound();
            }
            return View(la_pedidos);
        }

        // GET: Pedidos/Create
        public ActionResult Create()
        {
            ViewBag.id_usuario = new SelectList(db.la_usuarios, "id", "email");
            return View();
        }

        // POST: Pedidos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,costo_total,id_usuario,estado")] la_pedidos la_pedidos)
        {
            if (ModelState.IsValid)
            {
                db.la_pedidos.Add(la_pedidos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_usuario = new SelectList(db.la_usuarios, "id", "email", la_pedidos.id_usuario);
            return View(la_pedidos);
        }

        // GET: Pedidos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_pedidos la_pedidos = db.la_pedidos.Find(id);
            if (la_pedidos == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_usuario = new SelectList(db.la_usuarios, "id", "email", la_pedidos.id_usuario);
            return View(la_pedidos);
        }

        // POST: Pedidos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,costo_total,id_usuario,estado")] la_pedidos la_pedidos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(la_pedidos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_usuario = new SelectList(db.la_usuarios, "id", "email", la_pedidos.id_usuario);
            return View(la_pedidos);
        }

        // GET: Pedidos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_pedidos la_pedidos = db.la_pedidos.Find(id);
            if (la_pedidos == null)
            {
                return HttpNotFound();
            }
            return View(la_pedidos);
        }

        // POST: Pedidos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            la_pedidos la_pedidos = db.la_pedidos.Find(id);
            db.la_pedidos.Remove(la_pedidos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
