﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LiicoresApp_Admin.Models;

namespace LiicoresApp_Admin.Controllers
{
    public class UsuariosController : Controller
    {
        private LicoresAppEntities db = new LicoresAppEntities();

        // GET: Usuarios
        public ActionResult Index()
        {
            return View(db.la_usuarios.ToList());
        }

        // GET: Usuarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_usuarios la_usuarios = db.la_usuarios.Find(id);
            if (la_usuarios == null)
            {
                return HttpNotFound();
            }
            return View(la_usuarios);
        }

        // GET: Usuarios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Usuarios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,email,nombre,apellido")] la_usuarios la_usuarios)
        {
            if (ModelState.IsValid)
            {
                db.la_usuarios.Add(la_usuarios);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(la_usuarios);
        }

        // GET: Usuarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_usuarios la_usuarios = db.la_usuarios.Find(id);
            if (la_usuarios == null)
            {
                return HttpNotFound();
            }
            return View(la_usuarios);
        }

        // POST: Usuarios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,email,nombre,apellido")] la_usuarios la_usuarios)
        {
            if (ModelState.IsValid)
            {
                db.Entry(la_usuarios).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(la_usuarios);
        }

        // GET: Usuarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_usuarios la_usuarios = db.la_usuarios.Find(id);
            if (la_usuarios == null)
            {
                return HttpNotFound();
            }
            return View(la_usuarios);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            la_usuarios la_usuarios = db.la_usuarios.Find(id);
            db.la_usuarios.Remove(la_usuarios);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
