﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LiicoresApp_Admin.Models;

namespace LiicoresApp_Admin.Controllers
{
    public class ClientesController : Controller
    {
        private LicoresAppEntities db = new LicoresAppEntities();

        // GET: Clientes
        public ActionResult Index()
        {
            return View(db.la_clientes.ToList());
        }

        // GET: Clientes/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_clientes la_clientes = db.la_clientes.Find(id);
            if (la_clientes == null)
            {
                return HttpNotFound();
            }
            return View(la_clientes);
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clientes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_rut,nombre,email,password,habilitado,rating")] la_clientes la_clientes)
        {
            if (ModelState.IsValid)
            {
                db.la_clientes.Add(la_clientes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(la_clientes);
        }

        // GET: Clientes/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_clientes la_clientes = db.la_clientes.Find(id);
            if (la_clientes == null)
            {
                return HttpNotFound();
            }
            return View(la_clientes);
        }

        // POST: Clientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_rut,nombre,email,password,habilitado,rating")] la_clientes la_clientes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(la_clientes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(la_clientes);
        }

        // GET: Clientes/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            la_clientes la_clientes = db.la_clientes.Find(id);
            if (la_clientes == null)
            {
                return HttpNotFound();
            }
            return View(la_clientes);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            la_clientes la_clientes = db.la_clientes.Find(id);
            db.la_clientes.Remove(la_clientes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
