USE [LicoresApp]
GO
/****** Object:  Table [dbo].[la_usuarios]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[la_usuarios](
	[id] [int]  NOT NULL,
	[email] [varchar](100) NULL,
	[nombre] [varchar](100) NULL,
	[apellido] [varchar](100) NULL,
 CONSTRAINT [PK_la_usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[la_clientes]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[la_clientes](
	[id_rut] [varchar](20) NOT NULL,
	[nombre] [varchar](100) NULL,
	[email] [varchar](100) NULL,
	[password] [varchar](100) NULL,
	[habilitado] [tinyint] NULL,
	[rating] [tinyint] NULL,
 CONSTRAINT [PK_la_clientes] PRIMARY KEY CLUSTERED 
(
	[id_rut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[la_categorias]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[la_categorias](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NULL,
	[descripcion] [varchar](100) NULL,
 CONSTRAINT [PK_la_categorias] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[la_pedidos]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[la_pedidos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[costo_total] [int] NULL,
	[id_usuario] [int] NULL,
	[estado] [varchar](50) NULL,
 CONSTRAINT [PK_la_pedidos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[la_facturas_clientes]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[la_facturas_clientes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_cliente] [varchar](20) NULL,
	[fecha_emision] [date] NULL,
	[fecha_vencimiento] [date] NOT NULL,
	[deuda] [int] NULL,
	[estado] [varchar](50) NULL,
 CONSTRAINT [PK_la_facturas_clientes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[la_tiendas]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[la_tiendas](
	[id] [int] NOT NULL,
	[nombre] [varchar](50) NULL,
	[descripcion] [varchar](100) NULL,
	[direccion] [varchar](100) NULL,
	[imagen] [varchar](200) NULL,
	[id_cliente] [varchar](20) NULL,
 CONSTRAINT [PK_la_tiendas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[la_productos]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[la_productos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NULL,
	[descripcion] [varchar](100) NULL,
	[precio] [int] NULL,
	[imagen] [varchar](250) NULL,
	[id_categoria] [int] NULL,
 CONSTRAINT [PK_la_productos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[REGISTRO_CLIENTE]    Script Date: 06/22/2017 20:29:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[REGISTRO_CLIENTE]
	@id_rut varchar(20),
	@nombre varchar(100),
	@email varchar(100),
	@nombre_tienda varchar(50),
	@descripcion varchar(100),
	@direccion varchar(100),
	@imagen varchar(200)
	
AS

BEGIN TRANSACTION

   
  
	INSERT INTO dbo.la_clientes([id_rut],[nombre],[email],[password],[habilitado],[rating])
     VALUES(@id_rut,@nombre,@email,'',1,5)
     
     IF @@ERROR <> 0
 BEGIN
    -- Rollback the transaction
    ROLLBACK

    -- Raise an error and return
    RAISERROR ('ERROR AL CREAR EL CLIENTE.', 16, 1)
    RETURN
 END
     
    INSERT INTO dbo.la_tiendas([nombre],[descripcion],[direccion],[imagen],[id_cliente])
     VALUES(@nombre_tienda,@descripcion,@direccion,@imagen,@id_rut)


IF @@ERROR <> 0
 BEGIN
    -- Rollback the transaction
    ROLLBACK

    -- Raise an error and return
    RAISERROR ('ERROR AL CREAR LA TIENDA.', 16, 1)
    RETURN
 END
 
 COMMIT
GO
/****** Object:  Table [dbo].[la_tiendas_productos]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[la_tiendas_productos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_tienda] [int] NULL,
	[id_producto] [int] NULL,
	[stock_producto] [int] NULL,
 CONSTRAINT [PK_la_tiendas_productos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[la_pedidos_tiendas_productos]    Script Date: 06/22/2017 20:29:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[la_pedidos_tiendas_productos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_tiendas_productos] [int] NULL,
	[cantidad] [int] NULL,
	[precio_final] [int] NULL,
	[id_pedidos] [int] NULL,
 CONSTRAINT [PK_pedidos_tiendas_productos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]    Script Date: 06/22/2017 20:29:45 ******/
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]    Script Date: 06/22/2017 20:29:45 ******/
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]    Script Date: 06/22/2017 20:29:45 ******/
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]    Script Date: 06/22/2017 20:29:45 ******/
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
/****** Object:  ForeignKey [FK_la_facturas_clientes_la_clientes]    Script Date: 06/22/2017 20:29:45 ******/
ALTER TABLE [dbo].[la_facturas_clientes]  WITH CHECK ADD  CONSTRAINT [FK_la_facturas_clientes_la_clientes] FOREIGN KEY([id_cliente])
REFERENCES [dbo].[la_clientes] ([id_rut])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[la_facturas_clientes] CHECK CONSTRAINT [FK_la_facturas_clientes_la_clientes]
GO
/****** Object:  ForeignKey [FK_la_pedidos_la_usuarios]    Script Date: 06/22/2017 20:29:45 ******/
ALTER TABLE [dbo].[la_pedidos]  WITH CHECK ADD  CONSTRAINT [FK_la_pedidos_la_usuarios] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[la_usuarios] ([id])
GO
ALTER TABLE [dbo].[la_pedidos] CHECK CONSTRAINT [FK_la_pedidos_la_usuarios]
GO
/****** Object:  ForeignKey [FK_la_pedidos_tiendas_productos_la_pedidos]    Script Date: 06/22/2017 20:29:45 ******/
ALTER TABLE [dbo].[la_pedidos_tiendas_productos]  WITH CHECK ADD  CONSTRAINT [FK_la_pedidos_tiendas_productos_la_pedidos] FOREIGN KEY([id_pedidos])
REFERENCES [dbo].[la_pedidos] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[la_pedidos_tiendas_productos] CHECK CONSTRAINT [FK_la_pedidos_tiendas_productos_la_pedidos]
GO
/****** Object:  ForeignKey [FK_la_pedidos_tiendas_productos_la_tiendas_productos]    Script Date: 06/22/2017 20:29:45 ******/
ALTER TABLE [dbo].[la_pedidos_tiendas_productos]  WITH CHECK ADD  CONSTRAINT [FK_la_pedidos_tiendas_productos_la_tiendas_productos] FOREIGN KEY([id_tiendas_productos])
REFERENCES [dbo].[la_tiendas_productos] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[la_pedidos_tiendas_productos] CHECK CONSTRAINT [FK_la_pedidos_tiendas_productos_la_tiendas_productos]
GO
/****** Object:  ForeignKey [FK_la_productos_la_categorias1]    Script Date: 06/22/2017 20:29:45 ******/
ALTER TABLE [dbo].[la_productos]  WITH CHECK ADD  CONSTRAINT [FK_la_productos_la_categorias1] FOREIGN KEY([id_categoria])
REFERENCES [dbo].[la_categorias] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[la_productos] CHECK CONSTRAINT [FK_la_productos_la_categorias1]
GO
/****** Object:  ForeignKey [FK_la_tiendas_la_clientes]    Script Date: 06/22/2017 20:29:45 ******/
ALTER TABLE [dbo].[la_tiendas]  WITH CHECK ADD  CONSTRAINT [FK_la_tiendas_la_clientes] FOREIGN KEY([id_cliente])
REFERENCES [dbo].[la_clientes] ([id_rut])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[la_tiendas] CHECK CONSTRAINT [FK_la_tiendas_la_clientes]
GO
/****** Object:  ForeignKey [FK_la_tiendas_productos_la_productos]    Script Date: 06/22/2017 20:29:45 ******/
ALTER TABLE [dbo].[la_tiendas_productos]  WITH CHECK ADD  CONSTRAINT [FK_la_tiendas_productos_la_productos] FOREIGN KEY([id_producto])
REFERENCES [dbo].[la_productos] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[la_tiendas_productos] CHECK CONSTRAINT [FK_la_tiendas_productos_la_productos]
GO
/****** Object:  ForeignKey [FK_la_tiendas_productos_la_tiendas]    Script Date: 06/22/2017 20:29:45 ******/
ALTER TABLE [dbo].[la_tiendas_productos]  WITH CHECK ADD  CONSTRAINT [FK_la_tiendas_productos_la_tiendas] FOREIGN KEY([id_tienda])
REFERENCES [dbo].[la_tiendas] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[la_tiendas_productos] CHECK CONSTRAINT [FK_la_tiendas_productos_la_tiendas]
GO

USE LicoresApp
GO


/******************* TRIGGER PARA EL REGISTRO DE USUARIO POR MVC , FACEBOOK O GOOGLE   ***********************//
CREATE TRIGGER trigger_registrar_usuario ON LicoresApp.dbo.AspNetUsers
FOR INSERT AS
BEGIN

    INSERT INTO 
    la_usuarios
    (
        id,
		email,
        nombre
    )
    SELECT 
        Email, 
        UserName, 
        Id
    FROM 
        INSERTED
END