﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using DAL;
using System.Data;
using System.Web.Script.Serialization;

namespace WS_RegistroBartolo
{
    /// <summary>
    /// Summary description for WS_BartoloOnline
    /// </summary>
    [WebService(Namespace = "BartoloOnline")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WS_BartoloOnline : System.Web.Services.WebService
    {

        public int _verificarCupos(string clave)
        {
            RegistroBartolo _objRegBartolo = new RegistroBartolo
            {
                _ClaveOnlineID = clave
            };
            int cantidadCupos = int.Parse(_objRegBartolo.ClavesOnlineSel().Tables[0].Rows[0]["CUPOS_DISPONIBLES"].ToString());
            if (cantidadCupos > 0)
            {
                _objRegBartolo._ClaveCupos = cantidadCupos - 1;
                if (_objRegBartolo.ClaveCuposUPD() > 0)
                {
                    return 1;
                }
                return 2;
            }
            return 0;
        }

        private string consultaClienteClave(string clienteID, string claveID)
        {
            RegistroBartolo _objRegBartolo = new RegistroBartolo();
            try
            {
                _objRegBartolo._ClaveOnlineID = claveID;
                if (_objRegBartolo.ClavesOnlineSel().Tables[0].Rows.Count > 0)
                {
                    _objRegBartolo._ClienteID = clienteID;
                    DataSet _dsClavesClientes = _objRegBartolo.ClavesClientesSel();
                    if (_dsClavesClientes.Tables[0].Rows.Count > 0)
                    {
                        if (_dsClavesClientes.Tables[0].Rows[0]["ID_CLIENTE"].ToString().Equals(clienteID))
                        {
                            switch (this._verificarCupos(claveID))
                            {
                                case 0:
                                    return "1,1,0";

                                case 1:
                                    return "1,1,1";

                                case 2:
                                    return "0,0,0";
                            }
                            return "";
                        }
                        return "1,0,1";
                    }
                    if (_dsClavesClientes.Tables[1].Rows.Count <= 0)
                    {
                        return "0,0,1";
                    }
                    if (!_dsClavesClientes.Tables[1].Rows[0]["CLAVE_ONLINE"].ToString().Equals(""))
                    {
                        return "0,1,0";
                    }
                    _objRegBartolo._ClaveTelefonica = "";
                    if (_objRegBartolo.ClaveClientesUPD() > 0)
                    {
                        switch (this._verificarCupos(claveID))
                        {
                            case 0:
                                return "1,1,0";

                            case 1:
                                return "1,1,1";

                            case 2:
                                return "0,0,0";
                        }
                        return "";
                    }
                    return "0,0,0";
                }
                return "0,1,1";
            }
            catch (Exception)
            {
                return "0,0,0";
            }
        }

        [WebMethod]
        public string ConsultarRBDClave(string RBD, string Clave)
        {
            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            RespuestaConsultarRBDClave _consultaClave = new RespuestaConsultarRBDClave
            {
                RBD = RBD,
                CLAVE = Clave
            };
            try
            {
                _consultaClave.MENSAJE = this.consultaClienteClave(RBD, Clave);
                return oSerializer.Serialize(_consultaClave);
            }
            catch (Exception ex)
            {
                _consultaClave.MENSAJE = ex.Message;
                return oSerializer.Serialize(_consultaClave);
            }
        }

        [WebMethod]
        public string RegistrarClienteClaveOnline(string idCliente, string nombreCliente,
             string contactoNombre, string direccion, string comuna, string region, string telefonoEstablecimiento,
             string telefonoContacto, string correo, string claveOnline)     
           {

               if (region == "" || region==null)
               {
                   region = "Seleccione...";
               }

            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            RespuestaRegistrarRBDClave _registrarClave = new RespuestaRegistrarRBDClave
            {
                RBD = idCliente,
                NOMBRE = nombreCliente,
                DIRECCION = direccion,
                COMUNA = comuna,
                REGION = region,
                TELEFONO = telefonoEstablecimiento,
                NOMBRE_CONTACTO = contactoNombre,
                TELEFONO_CONTACTO = telefonoContacto,
                CORREO = correo,
                CLAVE = claveOnline
            };
            string estadoCliente = this.consultaClienteClave(idCliente, claveOnline);
            if (estadoCliente.Equals("0,0,1"))
            {
                try
                {
                    RegistroBartolo _objRegistroBartolo = new RegistroBartolo
                    {
                        _ClienteID = idCliente,
                        _NombreCliente = nombreCliente,
                        _Comuna =  comuna,
                        _Direccion = direccion,
                        _Region= region,
                        _TelefonoColegio = telefonoEstablecimiento,
                        _NombreContacto = contactoNombre,
                        _TelefonoContacto = telefonoContacto,
                        _EmailCliente = correo,
                        _ClaveOnlineID = claveOnline
                    };
                    /* ALTERACION PARA VERIFICAR - REGISTRO PUDO SER POR CLAVE TELEFONICA*/
                    int insUpdate = 0;

                    if (_objRegistroBartolo.ClienteSel().Tables[0].Rows.Count > 0)
                    {
                        insUpdate = _objRegistroBartolo.ClientesUpdate();
                    }
                    else
                    {
                        insUpdate = _objRegistroBartolo.ClientesInsert();
                    }
                    /* FIN ALTERACION PARA VERIFICAR - REGISTRO PUDO SER POR CLAVE TELEFONICA*/

                    if (insUpdate > 0)
                    {
                        _registrarClave.MENSAJE = this.consultaClienteClave(idCliente, claveOnline);
                        return oSerializer.Serialize(_registrarClave);
                    }
                    _registrarClave.MENSAJE = "CLIENTE YA REGISTRADO";
                    return oSerializer.Serialize(_registrarClave);
                }
                catch (Exception)
                {
                    _registrarClave.MENSAJE = "VERIFIQUE LOS DATOS";
                    return oSerializer.Serialize(_registrarClave);
                }
            }
            _registrarClave.MENSAJE = estadoCliente;
            return oSerializer.Serialize(_registrarClave);
        }


    }
}
