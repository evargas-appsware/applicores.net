﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Xml;
using System.ComponentModel;
using System.Web.Script.Serialization;
using DAL;


namespace WS_RegistroBartolo
{
    /// <summary>
    /// Summary description for WS_AvanceUsuarios
    /// </summary>
    [WebService(Namespace = "AvanceUsuarios")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WS_AvanceUsuarios : System.Web.Services.WebService
    {


        [WebMethod]
        public string deshabilitarUsuario(string idActivacion, string idCurso, string nombre, int anio)
        {
            string strResult = "";
            AvanceDeUsuarios avance = new AvanceDeUsuarios
            {
                _IdRegistro = idActivacion,
                _Curso = idCurso,
                _Nombre = nombre,
                _FechaCreacion = anio
            };
            DataSet _dsVerificar = avance.verificarHabilitado();
            if (_dsVerificar.Tables[0].Rows.Count > 0)
            {
                avance._IdUsuario = int.Parse(_dsVerificar.Tables[0].Rows[0][0].ToString());
                if (avance.habilitacionUsuarios(0) > 0)
                {
                    return "USUARIO DESHABILITADO";
                }
                return "ERROR DE CONEXION";
            }
            if (_dsVerificar.Tables[1].Rows.Count > 0)
            {
                strResult = "USUARIO YA ESTA DESHABILITADO";
                _dsVerificar = null;
                return strResult;
            }
            strResult = "USUARIO NO EXITE";
            _dsVerificar = null;
            return strResult;
        }


        public DatosRespuestaGuardarUsuarios guardarAvance(DatosGuardarAvanceUsuario _objUsuario, string idRegistro)
        {
            AvanceDeUsuarios avance = new AvanceDeUsuarios();
            JavaScriptSerializer _objSerializer = new JavaScriptSerializer();
            DatosRespuestaGuardarUsuarios respuestaUsuario = new DatosRespuestaGuardarUsuarios();
            string strMensaje = "";
            try
            {
                avance._IdRegistro = idRegistro;
                avance._Nombre = _objUsuario.NOMBRE;
                avance._Curso = _objUsuario.CURSO;
                avance._FechaCreacion = _objUsuario.FECHA;
                respuestaUsuario.NOMBRE = _objUsuario.NOMBRE;
                respuestaUsuario.CURSO = _objUsuario.CURSO;
                respuestaUsuario.FECHA = _objUsuario.FECHA;
                try
                {
                    avance._IdUsuario = avance.UsuarioUPD();
                }
                catch
                {
                    respuestaUsuario.RESULTADO = "ERROR CON LOS DATOS DEL USUARIO";
                    return respuestaUsuario;
                }
                if (avance._IdUsuario > 0)
                {
                    if (_objUsuario.CAPITULOS != null)
                    {
                        foreach (keyCapitulo _objCapitulo in _objUsuario.CAPITULOS)
                        {
                            if ((strMensaje != "GUARDAR OK") && (strMensaje != ""))
                            {
                                break;
                            }
                            avance._Capitulo = _objCapitulo.NUMERO;
                            foreach (keyCamino _objCamino in _objCapitulo.CAMINOS)
                            {
                                switch (strMensaje)
                                {
                                    case "GUARDAR OK":
                                    case "":
                                        avance._Camino = _objCamino.NUMERO;
                                        avance._CaminoCapituloSuperado = _objCamino.TERMINADO;
                                        avance._UsuarioCaminoId = avance.UsuariosCapitulosUPD();
                                        if (avance._UsuarioCaminoId > 0)
                                        {
                                            if (_objCamino.RUTAS != null)
                                            {
                                                foreach (keyRuta _objRuta in _objCamino.RUTAS)
                                                {
                                                    if ((strMensaje != "GUARDAR OK") && (strMensaje != ""))
                                                    {
                                                        break;
                                                    }
                                                    avance._Ruta = _objRuta.NUMERO;
                                                    foreach (keyActividad _objActividad in _objRuta.ACTIVIDADES)
                                                    {
                                                        avance._IdActividad = _objActividad.NUMERO;
                                                        avance._Puntos = _objActividad.PUNTOS;
                                                        avance._Superado = _objActividad.SUPERADO;
                                                        avance._UltimoAcceso = _objActividad.ULTIMO_ACCESO;
                                                        int flag = avance.UsuariosActividadesUPD();
                                                        if (avance._UsuarioCaminoId > 0)
                                                        {
                                                            strMensaje = "GUARDAR OK";
                                                        }
                                                        else
                                                        {
                                                            strMensaje = string.Concat(new object[] { "ERROR AL GUARDAR LA ACTIVIDAD \nCAPITULO: ", avance._Capitulo, "\nCAMINO: ", avance._Camino, "\nRUTA: ", avance._Ruta, "\nACTIVIDAD: ", avance._IdActividad });
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                strMensaje = "GUARDAR OK";
                                            }
                                            continue;
                                        }
                                        strMensaje = string.Concat(new object[] { "ERROR AL GUARDAR LA ACTIVIDAD \nCAPITULO: ", avance._Capitulo, "\nCAMINO: ", avance._Camino, "\nRUTA: ", avance._Ruta, "\nACTIVIDAD: ", avance._IdActividad });
                                        break;
                                }
                                break;
                            }
                        }
                    }
                    else
                    {
                        strMensaje = "GUARDAR OK";
                    }
                }
                else
                {
                    strMensaje = string.Concat(new object[] { "ERROR AL GUARDAR EL REGISTRO \nREGISTRO: ", avance._IdRegistro, "NOMBRE: ", avance._Nombre, "CURSO: ", avance._Curso, "FECHA: ", avance._FechaCreacion });
                }
                respuestaUsuario.RESULTADO = strMensaje;
                return respuestaUsuario;
            }
            catch (Exception ex)
            {
                respuestaUsuario.RESULTADO = ex.Message;
                return respuestaUsuario;
            }
        }

        [WebMethod]
        public string guardarAvanceMasivo(string strJSON)
        {
            GuardarAvanceVariosUsuarios _objUsuario = new GuardarAvanceVariosUsuarios();
            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            _objUsuario = oSerializer.Deserialize<GuardarAvanceVariosUsuarios>(strJSON);
            DatosRespuestaGuardarUsuarios[] ArrRespuesta = new DatosRespuestaGuardarUsuarios[_objUsuario.REGISTROS.Count<DatosGuardarAvanceUsuario>()];
            int flag = 0;
            foreach (DatosGuardarAvanceUsuario datosUsr in _objUsuario.REGISTROS)
            {
                DatosRespuestaGuardarUsuarios respuesta = new DatosRespuestaGuardarUsuarios();
                respuesta = this.guardarAvance(datosUsr, _objUsuario.ID_REGISTRO);
                ArrRespuesta.SetValue(respuesta, flag);
                flag++;
            }
            RespuestaGuradarVariosRegistros registros = new RespuestaGuradarVariosRegistros
            {
                REGISTROS = ArrRespuesta,
                ID_REGISTRO = _objUsuario.ID_REGISTRO
            };
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            return oSerializer.Serialize(registros);
        }


        [WebMethod]
        public string guardarAvanceSigle(string strJson)
        {
            DatosUsuario _objUsuario = new DatosUsuario();
            JavaScriptSerializer oSerializer = new JavaScriptSerializer();
            _objUsuario = oSerializer.Deserialize<DatosUsuario>(strJson);
            AvanceDeUsuarios avance = new AvanceDeUsuarios();
            DatosRespuestaGuardarUsuarios respuestaUsuario = new DatosRespuestaGuardarUsuarios();
            string strMensaje = "";
            try
            {
                avance._IdRegistro = _objUsuario.ID_REGISTRO;
                avance._Nombre = _objUsuario.NOMBRE;
                avance._Curso = _objUsuario.CURSO;
                avance._FechaCreacion = _objUsuario.FECHA;
                respuestaUsuario.NOMBRE = _objUsuario.NOMBRE;
                respuestaUsuario.CURSO = _objUsuario.CURSO;
                respuestaUsuario.FECHA = _objUsuario.FECHA;
                try
                {
                    avance._IdUsuario = avance.UsuarioUPD();
                }
                catch
                {
                    respuestaUsuario.RESULTADO = "ERROR CON LOS DATOS DEL USUARIO";
                    strJson = oSerializer.Serialize(respuestaUsuario);
                    return strJson;
                }
                if (avance._IdUsuario > 0)
                {
                    if (_objUsuario.CAPITULOS != null)
                    {
                        foreach (keyCapitulo _objCapitulo in _objUsuario.CAPITULOS)
                        {
                            if ((strMensaje != "GUARDAR OK") && (strMensaje != ""))
                            {
                                break;
                            }
                            avance._Capitulo = _objCapitulo.NUMERO;
                            foreach (keyCamino _objCamino in _objCapitulo.CAMINOS)
                            {
                                switch (strMensaje)
                                {
                                    case "GUARDAR OK":
                                    case "":
                                        avance._Camino = _objCamino.NUMERO;
                                        avance._CaminoCapituloSuperado = _objCamino.TERMINADO;
                                        avance._UsuarioCaminoId = avance.UsuariosCapitulosUPD();
                                        if (avance._UsuarioCaminoId > 0)
                                        {
                                            if (_objCamino.RUTAS != null)
                                            {
                                                foreach (keyRuta _objRuta in _objCamino.RUTAS)
                                                {
                                                    if ((strMensaje != "GUARDAR OK") && (strMensaje != ""))
                                                    {
                                                        break;
                                                    }
                                                    avance._Ruta = _objRuta.NUMERO;
                                                    foreach (keyActividad _objActividad in _objRuta.ACTIVIDADES)
                                                    {
                                                        avance._IdActividad = _objActividad.NUMERO;
                                                        avance._Puntos = _objActividad.PUNTOS;
                                                        avance._Superado = _objActividad.SUPERADO;
                                                        avance._UltimoAcceso = _objActividad.ULTIMO_ACCESO;
                                                        int flag = avance.UsuariosActividadesUPD();
                                                        if (avance._UsuarioCaminoId > 0)
                                                        {
                                                            strMensaje = "GUARDAR OK";
                                                        }
                                                        else
                                                        {
                                                            strMensaje = string.Concat(new object[] { "ERROR AL GUARDAR LA ACTIVIDAD \nCAPITULO: ", avance._Capitulo, "\nCAMINO: ", avance._Camino, "\nRUTA: ", avance._Ruta, "\nACTIVIDAD: ", avance._IdActividad });
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                strMensaje = "GUARDAR OK";
                                            }
                                            continue;
                                        }
                                        strMensaje = string.Concat(new object[] { "ERROR AL GUARDAR LA ACTIVIDAD \nCAPITULO: ", avance._Capitulo, "\nCAMINO: ", avance._Camino, "\nRUTA: ", avance._Ruta, "\nACTIVIDAD: ", avance._IdActividad });
                                        break;
                                }
                                break;
                            }
                        }
                    }
                    else
                    {
                        strMensaje = "GUARDAR OK";
                    }
                }
                else
                {
                    strMensaje = string.Concat(new object[] { "ERROR AL GUARDAR EL REGISTRO \nREGISTRO: ", avance._IdRegistro, "NOMBRE: ", avance._Nombre, "CURSO: ", avance._Curso, "FECHA: ", avance._FechaCreacion });
                }
                respuestaUsuario.RESULTADO = strMensaje;
                strJson = oSerializer.Serialize(respuestaUsuario);
                return strJson;
            }
            catch (Exception ex)
            {
                return ("ERROR EN EL FORMATO ENVIADO: " + ex.Message);
            }
        }

        [WebMethod]
        public string habilitarUsuario(string idActivacion, string idCurso, string nombre, int anio)
        {
            string strResult = "";
            AvanceDeUsuarios avance = new AvanceDeUsuarios
            {
                _IdRegistro = idActivacion,
                _Curso = idCurso,
                _Nombre = nombre,
                _FechaCreacion = anio
            };
            DataSet _dsVerificar = avance.verificarHabilitado();
            if (_dsVerificar.Tables[0].Rows.Count > 0)
            {
                strResult = "USUARIO YA ESTA HABILITADO";
                _dsVerificar = null;
                return strResult;
            }
            if (_dsVerificar.Tables[1].Rows.Count > 0)
            {
                avance._IdUsuario = int.Parse(_dsVerificar.Tables[1].Rows[0][0].ToString());
                if (avance.habilitacionUsuarios(1) > 0)
                {
                    return "USUARIO HABILITADO";
                }
                return "ERROR DE CONEXION";
            }
            return "USUARIO NO EXITE";
        }


        [WebMethod]
        public string obtenerAvanceMasivo(string idActivacion, string timeStamp)
        {
            string sJSON = "";
            AvanceDeUsuarios avance = new AvanceDeUsuarios
            {
                _IdRegistro = idActivacion,
                _TimeStamp = timeStamp
            };
            SincroAvanceVariosRegistros _sincroHistorico = new SincroAvanceVariosRegistros();
            DatosSincroAvanceUsuarios _Users = new DatosSincroAvanceUsuarios();
            keyCapitulo _objCapitulo = new keyCapitulo();
            keyCamino _objCamino = new keyCamino();
            keyRuta _objRuta = new keyRuta();
            keyActividad _objActividad = new keyActividad();
            try
            {
                DataSet _ds = avance.UsuariosAvanceMasivo();
                int _camino = 0;
                int _flagRegistros = 0;
                int _flagCapitulos = 0;
                int _flagCaminos = 0;
                int _flagRutas = 0;
                int _flagActividades = 0;
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    DatosSincroAvanceUsuarios[] _arrUsers = new DatosSincroAvanceUsuarios[_ds.Tables[0].Rows.Count];
                    foreach (DataRow _drUsuarios in _ds.Tables[0].Rows)
                    {
                        _flagCapitulos = 0;
                        _flagCaminos = 0;
                        _flagRutas = 0;
                        _flagActividades = 0;
                        _Users = new DatosSincroAvanceUsuarios
                        {
                            CURSO = _drUsuarios["CURSO"].ToString(),
                            NOMBRE = _drUsuarios["NOMBRE"].ToString(),
                            FECHA = int.Parse(_drUsuarios["A\x00d1O"].ToString()),
                            HABILITADO = int.Parse(_drUsuarios["HABILITADO"].ToString())
                        };
                        if (_Users.HABILITADO > 0)
                        {
                            try
                            {
                                DataTable _dtSupCapitulos = DataTableExtensions.CopyToDataTable<DataRow>(_ds.Tables[1].Select("ID_USUARIO=" + _drUsuarios["ID_USUARIO"].ToString()));
                                DataTable _dtSupCaminos = DataTableExtensions.CopyToDataTable<DataRow>(_ds.Tables[2].Select("ID_USUARIO=" + _drUsuarios["ID_USUARIO"].ToString()));
                                keyCapitulo[] _arrCapitulo = new keyCapitulo[_dtSupCapitulos.Rows.Count];
                                foreach (DataRow _rowCapitulos in _dtSupCapitulos.Rows)
                                {
                                    _objCapitulo = new keyCapitulo
                                    {
                                        NUMERO = int.Parse(_rowCapitulos["CAPITULO"].ToString())
                                    };
                                    DataTable _dtCamino = DataTableExtensions.CopyToDataTable<DataRow>(_dtSupCaminos.Select("CAPITULO=" + _rowCapitulos["CAPITULO"]));
                                    keyCamino[] _arrCamino = new keyCamino[_dtCamino.Rows.Count];
                                    _flagCaminos = 0;
                                    foreach (DataRow _rowCaminos in _dtCamino.Rows)
                                    {
                                        _objCamino = new keyCamino();
                                        _camino = int.Parse(_rowCaminos["CAMINO"].ToString());
                                        _objCamino.NUMERO = _camino;
                                        _objCamino.TERMINADO = int.Parse(_rowCaminos["TERMINADO"].ToString());
                                        DataTable _dtRutas = new DataTable();
                                        try
                                        {
                                            _dtRutas = DataTableExtensions.CopyToDataTable<DataRow>(DataTableExtensions.CopyToDataTable<DataRow>(_ds.Tables[3].Select("ID_USUARIO=" + _drUsuarios["ID_USUARIO"].ToString())).Select("ID_USUARIO_CAMINO=" + _rowCaminos["ID_USUARIO_CAMINO"]));
                                            DataTable _dtRowsRutas = new DataView(_dtRutas).ToTable(true, new string[] { "RUTA" });
                                            keyRuta[] _arrRuta = new keyRuta[_dtRowsRutas.Rows.Count];
                                            _flagRutas = 0;
                                            foreach (DataRow _rowRutas in _dtRowsRutas.Rows)
                                            {
                                                _objRuta = new keyRuta
                                                {
                                                    NUMERO = int.Parse(_rowRutas["RUTA"].ToString())
                                                };
                                                DataTable _dtActividades = DataTableExtensions.CopyToDataTable<DataRow>(_dtRutas.Select("RUTA=" + _objRuta.NUMERO));
                                                keyActividad[] _arrActividad = new keyActividad[_dtActividades.Rows.Count];
                                                _flagActividades = 0;
                                                foreach (DataRow _rowActividades in _dtActividades.Rows)
                                                {
                                                    _objActividad = new keyActividad
                                                    {
                                                        NUMERO = int.Parse(_rowActividades["ACTIVIDAD"].ToString()),
                                                        PUNTOS = int.Parse(_rowActividades["PUNTOS"].ToString()),
                                                        SUPERADO = int.Parse(_rowActividades["SUPERADO"].ToString()),
                                                        ULTIMO_ACCESO = int.Parse(_rowActividades["ULTIMO_ACCESO"].ToString())
                                                    };
                                                    _arrActividad.SetValue(_objActividad, _flagActividades);
                                                    _flagActividades++;
                                                }
                                                _objRuta.ACTIVIDADES = _arrActividad;
                                                _arrRuta.SetValue(_objRuta, _flagRutas);
                                                _flagRutas++;
                                            }
                                            _objCamino.RUTAS = _arrRuta;
                                        }
                                        catch
                                        {
                                        }
                                        _arrCamino.SetValue(_objCamino, _flagCaminos);
                                        _flagCaminos++;
                                    }
                                    _objCapitulo.CAMINOS = _arrCamino;
                                    _arrCapitulo.SetValue(_objCapitulo, _flagCapitulos);
                                    _flagCapitulos++;
                                }
                                _Users.CAPITULOS = _arrCapitulo;
                            }
                            catch
                            {
                                _Users.CAPITULOS = null;
                            }
                            _arrUsers.SetValue(_Users, _flagRegistros);
                            _flagRegistros++;
                        }
                    }
                    _sincroHistorico.ID_REGISTRO = idActivacion;
                    _sincroHistorico.REGISTROS = _arrUsers;
                    _sincroHistorico.TIMESTAMP = _ds.Tables[0].Rows[0]["STAMP_ACTUAL"].ToString();
                    JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                    sJSON = oSerializer.Serialize(_sincroHistorico);
                    DatosUsuario obj = oSerializer.Deserialize<DatosUsuario>(sJSON);
                    return sJSON;
                }
                return "NO EXISTEN AVANCES PARA MOSTRAR";
            }
            catch (Exception ex)
            {
                return ("ERROR AL OBTENER LOS DATOS : " + ex.Message);
            }
        }

        [WebMethod]
        public string obtenerAvanceSingle(string idActivacion, string idCurso, string nombre, int anio)
        {
            string sJSON = "";
            AvanceDeUsuarios avance = new AvanceDeUsuarios
            {
                _IdRegistro = idActivacion,
                _Curso = idCurso,
                _Nombre = nombre,
                _FechaCreacion = anio
            };
            DatosUsuario _Users = new DatosUsuario
            {
                ID_REGISTRO = idActivacion,
                CURSO = idCurso,
                NOMBRE = nombre,
                FECHA = anio
            };
            keyCapitulo _objCapitulo = new keyCapitulo();
            keyCamino _objCamino = new keyCamino();
            keyRuta _objRuta = new keyRuta();
            keyActividad _objActividad = new keyActividad();
            try
            {
                if (avance.verificarHabilitado().Tables[1].Rows.Count > 0)
                {
                    sJSON = "USUARIO ESTA DESHABILITADO";
                    return sJSON;
                }
                DataSet _ds = avance.UsuariosAvanceSingle();
                int _capitulo = 0;
                int _camino = 0;
                int _flagCapitulos = 0;
                int _flagCaminos = 0;
                int _flagRutas = 0;
                int _flagActividades = 0;
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    _capitulo = int.Parse(_ds.Tables[0].Rows[0]["CAPITULO"].ToString());
                    keyCapitulo[] _arrCapitulo = new keyCapitulo[_ds.Tables[0].Rows.Count];
                    foreach (DataRow _rowCapitulos in _ds.Tables[0].Rows)
                    {
                        _objCapitulo = new keyCapitulo
                        {
                            NUMERO = int.Parse(_rowCapitulos["CAPITULO"].ToString())
                        };
                        DataTable _dtCamino = DataTableExtensions.CopyToDataTable<DataRow>(_ds.Tables[1].Select("CAPITULO=" + _rowCapitulos["CAPITULO"]));
                        keyCamino[] _arrCamino = new keyCamino[_dtCamino.Rows.Count];
                        _flagCaminos = 0;
                        foreach (DataRow _rowCaminos in _dtCamino.Rows)
                        {
                            _objCamino = new keyCamino();
                            _camino = int.Parse(_rowCaminos["CAMINO"].ToString());
                            _objCamino.NUMERO = _camino;
                            _objCamino.TERMINADO = int.Parse(_rowCaminos["TERMINADO"].ToString());
                            DataTable _dtRutas = new DataTable();
                            if (_ds.Tables[2].Select("ID_USUARIO_CAMINO=" + _rowCaminos["ID_USUARIO_CAMINO"]).Count<DataRow>() > 0)
                            {
                                _dtRutas = DataTableExtensions.CopyToDataTable<DataRow>(_ds.Tables[2].Select("ID_USUARIO_CAMINO=" + _rowCaminos["ID_USUARIO_CAMINO"]));
                                DataTable _dtRowsRutas = new DataView(_dtRutas).ToTable(true, new string[] { "RUTA" });
                                keyRuta[] _arrRuta = new keyRuta[_dtRowsRutas.Rows.Count];
                                _flagRutas = 0;
                                foreach (DataRow _rowRutas in _dtRowsRutas.Rows)
                                {
                                    _objRuta = new keyRuta
                                    {
                                        NUMERO = int.Parse(_rowRutas["RUTA"].ToString())
                                    };
                                    DataTable _dtActividades = DataTableExtensions.CopyToDataTable<DataRow>(_dtRutas.Select("RUTA=" + _objRuta.NUMERO));
                                    keyActividad[] _arrActividad = new keyActividad[_dtActividades.Rows.Count];
                                    _flagActividades = 0;
                                    foreach (DataRow _rowActividades in _dtActividades.Rows)
                                    {
                                        _objActividad = new keyActividad
                                        {
                                            NUMERO = int.Parse(_rowActividades["ACTIVIDAD"].ToString()),
                                            PUNTOS = int.Parse(_rowActividades["PUNTOS"].ToString()),
                                            SUPERADO = int.Parse(_rowActividades["SUPERADO"].ToString()),
                                            ULTIMO_ACCESO = int.Parse(_rowActividades["ULTIMO_ACCESO"].ToString())
                                        };
                                        _arrActividad.SetValue(_objActividad, _flagActividades);
                                        _flagActividades++;
                                    }
                                    _objRuta.ACTIVIDADES = _arrActividad;
                                    _arrRuta.SetValue(_objRuta, _flagRutas);
                                    _flagRutas++;
                                }
                                _objCamino.RUTAS = _arrRuta;
                            }
                            _arrCamino.SetValue(_objCamino, _flagCaminos);
                            _flagCaminos++;
                        }
                        _objCapitulo.CAMINOS = _arrCamino;
                        _arrCapitulo.SetValue(_objCapitulo, _flagCapitulos);
                        _flagCapitulos++;
                    }
                    _Users.CAPITULOS = _arrCapitulo;
                    JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                    return oSerializer.Serialize(_Users);
                }
                if (_ds.Tables.Count == 1)
                {
                    return "USUARIO NO EXISTE";
                }
                return "USUARIO NO POSEE AVANCE";
            }
            catch (Exception ex)
            {
                return ("ERROR DE CONEXION: " + ex.Message);
            }
        }

        [WebMethod]
        public string obtenerPuntajesTotales(string idActivacion, string timeStamp)
        {
            AvanceDeUsuarios avance = new AvanceDeUsuarios
            {
                _IdRegistro = idActivacion,
                _TimeStamp = timeStamp
            };
            SincroPuntajesTotales _sincroPuntaje = new SincroPuntajesTotales();
            int contadorRegistros = 0;
            try
            {
                DataSet _ds = avance.HistoricoPuntajeUsuarios();
                contadorRegistros = _ds.Tables[0].Rows.Count;
                if (contadorRegistros > 0)
                {
                    PuntajeDeUsuarios[] arrPuntajeUsers = new PuntajeDeUsuarios[contadorRegistros];
                    for (int flag = 0; flag < contadorRegistros; flag++)
                    {
                        PuntajeDeUsuarios _Users = new PuntajeDeUsuarios
                        {
                            CURSO = _ds.Tables[0].Rows[flag]["CURSO"].ToString(),
                            FECHA = int.Parse(_ds.Tables[0].Rows[flag]["A\x00d1O"].ToString()),
                            NOMBRE = _ds.Tables[0].Rows[flag]["NOMBRE"].ToString(),
                            HABILITADO = int.Parse(_ds.Tables[0].Rows[flag]["HABILITADO"].ToString())
                        };
                        int idUsuario = int.Parse(_ds.Tables[0].Rows[flag]["ID_USUARIO"].ToString());
                        int count = _ds.Tables[1].Select("ID_USUARIO=" + idUsuario).Count<DataRow>();
                        if ((_Users.HABILITADO > 0) && (_ds.Tables[1].Select("ID_USUARIO=" + idUsuario).Count<DataRow>() > 0))
                        {
                            DataTable dtPuntos = new DataView(DataTableExtensions.CopyToDataTable<DataRow>(_ds.Tables[1].Select("ID_USUARIO=" + idUsuario))).ToTable();
                            _Users.PUNTOS = int.Parse(dtPuntos.Rows[0]["PUNTOS"].ToString());
                        }
                        else
                        {
                            _Users.PUNTOS = 0;
                        }
                        arrPuntajeUsers.SetValue(_Users, flag);
                    }
                    _sincroPuntaje.ID_REGISTRO = idActivacion;
                    _sincroPuntaje.TIMESTAMP = _ds.Tables[2].Rows[0]["STAMP_ACTUAL"].ToString();
                    _sincroPuntaje.REGISTROS = arrPuntajeUsers;
                    JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                    return oSerializer.Serialize(_sincroPuntaje);
                }
                return "NO EXISTEN PUNTAJES PARA MOSTRAR";
            }
            catch (Exception ex)
            {
                return ("ERROR DE CONEXION: " + ex.Message);
            }
        }

        [WebMethod]
        public string obtenerPuntajesCapitulos(string idActivacion, string timeStamp)
        {
            AvanceDeUsuarios avance = new AvanceDeUsuarios
            {
                _IdRegistro = idActivacion,
                _TimeStamp = timeStamp
            };
            SincroPuntajesCamino _sincroPuntaje = new SincroPuntajesCamino();
            int contadorRegistros = 0;
            try
            {
                DataSet _ds = avance.HistoricoPuntajeUsuariosCapitulos();
                contadorRegistros = _ds.Tables[0].Rows.Count;
                if (contadorRegistros > 0)
                {
                    PuntajeDeUsuariosCaminos[] arrPuntajeUsers = new PuntajeDeUsuariosCaminos[contadorRegistros];
                    for (int flag = 0; flag < contadorRegistros; flag++)
                    {
                        PuntajeDeUsuariosCaminos _Users = new PuntajeDeUsuariosCaminos
                        {
                            CURSO = _ds.Tables[0].Rows[flag]["CURSO"].ToString(),
                            FECHA = int.Parse(_ds.Tables[0].Rows[flag]["AÑO"].ToString()),
                            NOMBRE = _ds.Tables[0].Rows[flag]["NOMBRE"].ToString(),
                            HABILITADO = int.Parse(_ds.Tables[0].Rows[flag]["HABILITADO"].ToString())
                        };
                        int idUsuario = int.Parse(_ds.Tables[0].Rows[flag]["ID_USUARIO"].ToString());
                        int count = _ds.Tables[1].Select("ID_USUARIO=" + idUsuario).Count<DataRow>();
                        if ((_Users.HABILITADO > 0) && (_ds.Tables[1].Select("ID_USUARIO=" + idUsuario).Count<DataRow>() > 0))
                        {

                            
                            DataTable dtPuntos = new DataView(DataTableExtensions.CopyToDataTable<DataRow>(_ds.Tables[1].Select("ID_USUARIO=" + idUsuario))).ToTable();
                           
                            puntosCamino[] CAPITULOS = new puntosCamino[dtPuntos.Rows.Count];

                            for (int flagCaminos = 0; flagCaminos < dtPuntos.Rows.Count; flagCaminos++)
                            {
                                puntosCamino CAPITULO = new puntosCamino();
                                CAPITULO.NUMERO = Int32.Parse(dtPuntos.Rows[flagCaminos]["CAPITULO"].ToString());
                                CAPITULO.PUNTOS = Int32.Parse(dtPuntos.Rows[flagCaminos]["PUNTOS"].ToString());

                                CAPITULOS.SetValue(CAPITULO, flagCaminos);
                            }
                            _Users.CAPITULOS = CAPITULOS;
                        }
                        else
                        {
                            _Users.CAPITULOS = null;
                        }
                        arrPuntajeUsers.SetValue(_Users, flag);
                    }
                    _sincroPuntaje.ID_REGISTRO = idActivacion;
                    _sincroPuntaje.TIMESTAMP = _ds.Tables[2].Rows[0]["STAMP_ACTUAL"].ToString();
                    _sincroPuntaje.REGISTROS = arrPuntajeUsers;
                    JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                    return oSerializer.Serialize(_sincroPuntaje);
                }
                return "NO EXISTEN PUNTAJES PARA MOSTRAR";
            }
            catch (Exception ex)
            {
                return ("ERROR DE CONEXION: " + ex.Message);
            }
        }



        [WebMethod]
        public string obtenerUsuariosDeshabilitados(string idActivacion, string timeStamp)
        {
            AvanceDeUsuarios avance = new AvanceDeUsuarios
            {
                _IdRegistro = idActivacion,
                _TimeStamp = timeStamp
            };
            ListaDeUsuariosDeshabilitados ListasDeUsuarios = new ListaDeUsuariosDeshabilitados();
            int contadorRegistros = 0;
            try
            {
                DataSet _ds = avance.ListaUsuariosDeshabilitados();
                if (_ds.Tables[0].Rows.Count > 0)
                {
                    DataTable _usuariosDeshabilitados = _ds.Tables[0].Clone();
                    foreach (DataRow _drListado in _ds.Tables[0].Rows)
                    {
                        avance._Curso = _drListado["CURSO"].ToString();
                        avance._FechaCreacion = int.Parse(_drListado["AÑO"].ToString());
                        avance._Nombre = _drListado["NOMBRE"].ToString();
                        avance._IdRegistro = idActivacion;
                        if (avance.verificarHabilitado().Tables[0].Rows.Count == 0)
                        {
                            DataRow _drTemp = _drListado;
                            _usuariosDeshabilitados.ImportRow(_drTemp);
                        }
                    }
                    contadorRegistros = _usuariosDeshabilitados.Rows.Count;
                    UsuariosDeshabilitado[] arrUsersDeshabilitado = new UsuariosDeshabilitado[contadorRegistros];
                    for (int flag = 0; flag < contadorRegistros; flag++)
                    {
                        UsuariosDeshabilitado _Users = new UsuariosDeshabilitado
                        {
                            CURSO = _usuariosDeshabilitados.Rows[flag]["CURSO"].ToString(),
                            FECHA = int.Parse(_usuariosDeshabilitados.Rows[flag]["A\x00d1O"].ToString()),
                            NOMBRE = _usuariosDeshabilitados.Rows[flag]["NOMBRE"].ToString(),
                            HABILITADO = int.Parse(_usuariosDeshabilitados.Rows[flag]["HABILITADO"].ToString())
                        };
                        arrUsersDeshabilitado.SetValue(_Users, flag);
                    }
                    ListasDeUsuarios.ID_REGISTRO = idActivacion;
                    ListasDeUsuarios.TIMESTAMP = _ds.Tables[1].Rows[0]["STAMP_ACTUAL"].ToString();
                    ListasDeUsuarios.REGISTROS = arrUsersDeshabilitado;
                    JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                    return oSerializer.Serialize(ListasDeUsuarios);
                }
                return "NO EXISTEN USUARIOS DESHABILITADOS PARA MOSTRAR";
            }
            catch (Exception ex)
            {
                return ("ERROR DE CONEXION: " + ex.Message);
            }
        }



    }
}
