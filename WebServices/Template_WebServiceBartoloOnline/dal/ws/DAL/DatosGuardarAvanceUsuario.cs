﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class DatosGuardarAvanceUsuario
    {
        public keyCapitulo[] CAPITULOS { get; set; }

        public string CURSO { get; set; }

        public int FECHA { get; set; }

        public string NOMBRE { get; set; }
    }
}

