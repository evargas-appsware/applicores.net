﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class keyCamino
    {
        public int NUMERO { get; set; }

        public keyRuta[] RUTAS { get; set; }

        public int TERMINADO { get; set; }
    }
}

