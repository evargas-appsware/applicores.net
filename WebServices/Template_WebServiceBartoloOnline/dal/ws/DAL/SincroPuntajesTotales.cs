﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class SincroPuntajesTotales
    {
        public string ID_REGISTRO { get; set; }

        public PuntajeDeUsuarios[] REGISTROS { get; set; }

        public string TIMESTAMP { get; set; }
    }
}

