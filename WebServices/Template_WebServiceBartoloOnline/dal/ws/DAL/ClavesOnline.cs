﻿namespace DAL
{
    using System;
    using System.Collections;
    using System.Runtime.InteropServices;

    public class ClavesOnline
    {
        private int rotacionTemp = 0;
        private static string SECUENCIA = "cadvequtrjfxmpnwhky134567890";

        private string CodificarNumero(int num, int digitos = 0, int rotacion = 0)
        {
            try
            {
                int _totalDigitos = SECUENCIA.Length;
                ArrayList _resultado = new ArrayList();
                while (num != 0)
                {
                    _resultado.Add(SECUENCIA.Substring(this.rotarNumero(num % _totalDigitos, rotacion), 1));
                    num = (int) Math.Floor(Convert.ToDecimal((int) (num / _totalDigitos)));
                }
                string strAux = "";
                for (int flag = _resultado.Count - 1; flag >= 0; flag--)
                {
                    strAux = strAux + _resultado[flag].ToString();
                }
                if (digitos > 0)
                {
                    while (strAux.Length < digitos)
                    {
                        strAux = SECUENCIA.Substring(this.rotarNumero(0, rotacion), 1) + strAux;
                    }
                    if (strAux.Length > digitos)
                    {
                        strAux = strAux.Substring(strAux.Length - digitos, digitos);
                    }
                }
                return strAux;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string generarClave(DateTime _fechaActual, string tipo)
        {
            try
            {
                DateTime _fechaAño = new DateTime(_fechaActual.Year, 1, 1);
                string fecha = (((_fechaAño.ToUniversalTime().Ticks - 0x89f7ff5f7b58000L) / 0x989680L) / 60L).ToString();
                int _totalDigitos = SECUENCIA.Length;
                long _minutosAño = ((_fechaAño.ToUniversalTime().Ticks - 0x89f7ff5f7b58000L) / 0x989680L) / 60L;
                long _minutos = (((_fechaActual.ToUniversalTime().Ticks - 0x89f7ff5f7b58000L) / 0x989680L) / 60L) - _minutosAño;
                Random auxRandom = new Random();
                int auxNum = auxRandom.Next(1, 10);
                int factor = auxRandom.Next(1, 10);
                int rotacion = auxRandom.Next(1, _totalDigitos);
                while (rotacion == this.rotacionTemp)
                {
                    rotacion = auxRandom.Next(1, _totalDigitos);
                    factor = auxRandom.Next(1, 10);
                    auxNum = auxRandom.Next(1, 10);
                }
                this.rotacionTemp = rotacion;
                string rotacionStr = this.CodificarNumero(rotacion, 0, 0);
                string factotStr = this.CodificarNumero(factor, 1, rotacion);
                string minutosStr = this.CodificarNumero(Convert.ToInt32(Math.Round(Convert.ToDecimal((long) (_minutos * factor)))), 5, rotacion);
                int maxMultiplo = Convert.ToInt32(Math.Floor((decimal) (Convert.ToDecimal((int) (_totalDigitos - 1)) / factor)));
                string relleno = this.CodificarNumero(auxRandom.Next(maxMultiplo + 1) * factor, 1, rotacion) + this.CodificarNumero(auxRandom.Next(maxMultiplo + 1) * factor, 1, rotacion) + this.CodificarNumero(auxRandom.Next(maxMultiplo + 1) * factor, 1, rotacion) + this.CodificarNumero(auxRandom.Next(maxMultiplo + 1) * factor, 1, rotacion);
                string clave = factotStr + rotacionStr + minutosStr + tipo + relleno;
                return (clave.Substring(0, 4) + "-" + clave.Substring(4, 4) + "-" + clave.Substring(8, 4)).ToUpper();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private int rotarNumero(int num, int rotacion)
        {
            if (rotacion == 0)
            {
                return num;
            }
            int resultado = num + rotacion;
            while (resultado < 0)
            {
                resultado += SECUENCIA.Length;
            }
            return (resultado % SECUENCIA.Length);
        }
    }
}

