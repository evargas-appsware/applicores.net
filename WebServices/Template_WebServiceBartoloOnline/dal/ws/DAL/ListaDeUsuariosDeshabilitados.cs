﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ListaDeUsuariosDeshabilitados
    {
        public string ID_REGISTRO { get; set; }

        public UsuariosDeshabilitado[] REGISTROS { get; set; }

        public string TIMESTAMP { get; set; }
    }
}

