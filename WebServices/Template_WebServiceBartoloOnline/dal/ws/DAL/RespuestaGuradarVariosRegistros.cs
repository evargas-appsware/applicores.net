﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    public class RespuestaGuradarVariosRegistros
    {
        public string ID_REGISTRO { get; set; }

        public DatosRespuestaGuardarUsuarios[] REGISTROS { get; set; }
    }
}

