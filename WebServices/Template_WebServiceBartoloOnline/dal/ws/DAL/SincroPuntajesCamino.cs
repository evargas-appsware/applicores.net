﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class SincroPuntajesCamino
    {
        public string ID_REGISTRO { get; set; }

        public PuntajeDeUsuariosCaminos[] REGISTROS { get; set; }

        public string TIMESTAMP { get; set; }
    }
}

