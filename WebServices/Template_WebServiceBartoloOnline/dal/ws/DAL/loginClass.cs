﻿namespace DAL
{
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Diagnostics;

    public class loginClass
    {
        private string clave;
        private string user;

        public int LoginVerfication()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_ADMIN_SEL");
                _db.AddInParameter(_command, "@NOMBRE", DbType.String, this._User);
                _db.AddInParameter(_command, "@CLAVE", DbType.String, this._Clave);
                return Convert.ToInt32(_db.ExecuteDataSet(_command).Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return 0;
            }
        }

        public string _Clave
        {
            get
            {
                return this.clave;
            }
            set
            {
                this.clave = value;
            }
        }

        public string _User
        {
            get
            {
                return this.user;
            }
            set
            {
                this.user = value;
            }
        }
    }
}

