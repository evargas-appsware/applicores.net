﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class RespuestaRegistrarRBDClave
    {
        public string CLAVE { get; set; }

        public string CORREO { get; set; }

        public string MENSAJE { get; set; }

        public string NOMBRE { get; set; }

        public string NOMBRE_CONTACTO { get; set; }

        public string RBD { get; set; }

        public string TELEFONO_CONTACTO { get; set; }
    }
}

