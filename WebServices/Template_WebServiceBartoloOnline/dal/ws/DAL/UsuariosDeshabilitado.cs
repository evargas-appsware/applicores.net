﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class UsuariosDeshabilitado
    {
        public string CURSO { get; set; }

        public int FECHA { get; set; }

        public int HABILITADO { get; set; }

        public string NOMBRE { get; set; }
    }
}

