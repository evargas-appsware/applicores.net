﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class keyActividad
    {
        public int NUMERO { get; set; }

        public int PUNTOS { get; set; }

        public int SUPERADO { get; set; }

        public int ULTIMO_ACCESO { get; set; }
    }
}

