﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuardarAvanceVariosUsuarios
    {
        public string ID_REGISTRO { get; set; }

        public DatosGuardarAvanceUsuario[] REGISTROS { get; set; }
    }
}

