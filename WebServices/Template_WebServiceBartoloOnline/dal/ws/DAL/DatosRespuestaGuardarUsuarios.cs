﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class DatosRespuestaGuardarUsuarios
    {
        public string CURSO { get; set; }

        public int FECHA { get; set; }

        public string NOMBRE { get; set; }

        public string RESULTADO { get; set; }
    }
}

