﻿namespace DAL
{
    using System;
    using System.Web.UI.WebControls;

    public class Funciones
    {
        public void MensajeAlerta(ref Literal LMsg, string Msj)
        {
            if (Msj == "")
            {
                LMsg.Text = "";
            }
            else
            {
                LMsg.Text = "<div id=\"Alerta\"><p>";
                LMsg.Text = LMsg.Text + Msj;
                LMsg.Text = LMsg.Text + "</p></div>";
            }
        }

        public void MensajeAviso(ref Literal LMsg, string Msj)
        {
            if (Msj == "")
            {
                LMsg.Text = "";
            }
            else
            {
                LMsg.Text = "<div id=\"AlertaAviso\"><p>";
                LMsg.Text = LMsg.Text + Msj;
                LMsg.Text = LMsg.Text + "</p></div>";
            }
        }

        public void MensajeAvisoAbsoluto(ref Literal LMsg, string Msj)
        {
            if (Msj == "")
            {
                LMsg.Text = "";
            }
            else
            {
                LMsg.Text = "<div id=\"AlertaAvisoAbsoluto\"><p>";
                LMsg.Text = LMsg.Text + Msj;
                LMsg.Text = LMsg.Text + "</p></div>";
            }
        }

        public void MensajeSatistactorio(ref Literal LMsg, string Msj)
        {
            if (Msj == "")
            {
                LMsg.Text = "";
            }
            else
            {
                LMsg.Text = "<div id=\"AlertaSatisfactoria\"><p>";
                LMsg.Text = LMsg.Text + Msj;
                LMsg.Text = LMsg.Text + "</p></div>";
            }
        }
    }
}

