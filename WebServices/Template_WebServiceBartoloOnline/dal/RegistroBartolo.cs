﻿namespace DAL
{
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Diagnostics;

    public class RegistroBartolo
    {
        private int claveCupos;
        private string claveOnlineID = "";
        private string claveTelefonica;
        private string clienteID;
        private string comuna;
        private string emailCliente;
        private string nombreCliente;
        private string nombreContacto;
        private string direccion;
        private string telefonoContacto;
        private string telefonoColegio;
        private string region;


        public int ClaveClientesINS()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_CLAVES_CLIENTES_INS");
                _db.AddInParameter(_command, "@ID_CLIENTE", DbType.String, this._ClienteID);
                _db.AddInParameter(_command, "@CLAVE_OFFLINE", DbType.String, this._ClaveTelefonica);
                _db.AddInParameter(_command, "@CLAVE_ONLINE", DbType.String, this._ClaveOnlineID);
                return Convert.ToInt32(_db.ExecuteDataSet(_command).Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return 0;
            }
        }



        public int ClaveClientesUPD()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_CLAVES_CLIENTES_UPD");
                _db.AddInParameter(_command, "@ID_CLIENTE", DbType.String, this._ClienteID);
                _db.AddInParameter(_command, "@CLAVE_OFFLINE", DbType.String, this._ClaveTelefonica);
                _db.AddInParameter(_command, "@CLAVE_ONLINE", DbType.String, this._ClaveOnlineID);
                return Convert.ToInt32(_db.ExecuteDataSet(_command).Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return 0;
            }
        }




        public int ClaveCuposUPD()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_CLAVES_UPD");
                _db.AddInParameter(_command, "@CLAVE", DbType.String, this._ClaveOnlineID);
                _db.AddInParameter(_command, "@CUPOS", DbType.Int32, this._ClaveCupos);
                return Convert.ToInt32(_db.ExecuteDataSet(_command).Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return 0;
            }
        }

        public int ClaveOnlineINS()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_CLAVES_INS");
                _db.AddInParameter(_command, "@CLAVE", DbType.String, this._ClaveOnlineID);
                _db.AddInParameter(_command, "@CUPOS", DbType.String, this._ClaveCupos);
                return Convert.ToInt32(_db.ExecuteDataSet(_command).Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return 0;
            }
        }

        public DataSet ClavesClientesSel()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_CLAVES_CLIENTES_SEL");
                _db.AddInParameter(_command, "@ID_CLIENTE", DbType.String, this._ClienteID);
                _db.AddInParameter(_command, "@CLAVE", DbType.String, this._ClaveOnlineID);
                return _db.ExecuteDataSet(_command);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return null;
            }
        }

        public DataSet ClavesOnlineSel()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_CLAVES_SEL");
                _db.AddInParameter(_command, "@CLAVE", DbType.String, this._ClaveOnlineID);
                return _db.ExecuteDataSet(_command);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return null;
            }
        }

        public int ClaveTelefonicasINS()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_CLAVES_TELEFONICAS_INS");
                _db.AddInParameter(_command, "@CLAVE", DbType.String, this._ClaveTelefonica);
                _db.AddInParameter(_command, "@RBD", DbType.String, this._ClienteID);
                return Convert.ToInt32(_db.ExecuteDataSet(_command).Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return 0;
            }
        }

        public DataSet ClienteClaveTelefonicaSel(string rbd)
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_CLIENTE_CLAVE_TELEFONICA_SEL");
                _db.AddInParameter(_command, "@RBD", DbType.String, rbd);
                return _db.ExecuteDataSet(_command);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return null;
            }
        }

        public DataSet ClienteSel()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_CLIENTE_SEL");
                _db.AddInParameter(_command, "@ID_CLIENTE", DbType.String, this._ClienteID);
                return _db.ExecuteDataSet(_command);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return null;
            }
        }

        public int ClientesInsert()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_CLIENTE_INS");
                _db.AddInParameter(_command, "@ID_CLIENTE", DbType.String, this._ClienteID);
                _db.AddInParameter(_command, "@NOMBRE_CLIENTE", DbType.String, this._NombreCliente);
                _db.AddInParameter(_command, "@DIRECCION", DbType.String, this._Direccion);
                _db.AddInParameter(_command, "@COMUNA", DbType.String, this._Comuna);
                _db.AddInParameter(_command, "@REGION", DbType.String, this._Region);
                _db.AddInParameter(_command, "@TELEFONO", DbType.String, this._TelefonoColegio);
                _db.AddInParameter(_command, "@CONTACTO_NOMBRE", DbType.String, this._NombreContacto);
                _db.AddInParameter(_command, "@TELEFONO_CONTACTO", DbType.String, this._TelefonoContacto);
                _db.AddInParameter(_command, "@CORREO", DbType.String, this._EmailCliente);
                return Convert.ToInt32(_db.ExecuteDataSet(_command).Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return 0;
            }
        }

        public int ClientesUpdate()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_CLIENTE_UPD");
                _db.AddInParameter(_command, "@ID_CLIENTE", DbType.String, this._ClienteID);
                _db.AddInParameter(_command, "@NOMBRE_CLIENTE", DbType.String, this._NombreCliente);
                _db.AddInParameter(_command, "@DIRECCION", DbType.String, this._Direccion);
                _db.AddInParameter(_command, "@COMUNA", DbType.String, this._Comuna);
                _db.AddInParameter(_command, "@REGION", DbType.String, this._Region);
                _db.AddInParameter(_command, "@TELEFONO", DbType.String, this._TelefonoColegio);
                _db.AddInParameter(_command, "@CONTACTO_NOMBRE", DbType.String, this._NombreContacto);
                _db.AddInParameter(_command, "@TELEFONO_CONTACTO", DbType.String, this._TelefonoContacto);
                _db.AddInParameter(_command, "@CORREO", DbType.String, this._EmailCliente);
                return Convert.ToInt32(_db.ExecuteDataSet(_command).Tables[0].Rows[0][0]);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
                return 0;
            }
        }

        public int _ClaveCupos
        {
            get
            {
                return this.claveCupos;
            }
            set
            {
                this.claveCupos = value;
            }
        }

        public string _ClaveOnlineID
        {
            get
            {
                return this.claveOnlineID;
            }
            set
            {
                this.claveOnlineID = value;
            }
        }

        public string _ClaveTelefonica
        {
            get
            {
                return this.claveTelefonica;
            }
            set
            {
                this.claveTelefonica = value;
            }
        }

        public string _ClienteID
        {
            get
            {
                return this.clienteID;
            }
            set
            {
                this.clienteID = value;
            }
        }

        public string _Comuna
        {
            get
            {
                return this.comuna;
            }
            set
            {
                this.comuna = value;
            }
        }

        public string _EmailCliente
        {
            get
            {
                return this.emailCliente;
            }
            set
            {
                this.emailCliente = value;
            }
        }

        public string _NombreCliente
        {
            get
            {
                return this.nombreCliente;
            }
            set
            {
                this.nombreCliente = value;
            }
        }

        public string _NombreContacto
        {
            get
            {
                return this.nombreContacto;
            }
            set
            {
                this.nombreContacto = value;
            }
        }

        public string _Direccion
        {
            get
            {
                return this.direccion;
            }
            set
            {
                this.direccion = value;
            }
        }

        public string _TelefonoContacto
        {
            get
            {
                return this.telefonoContacto;
            }
            set
            {
                this.telefonoContacto = value;
            }
        }

        public string _TelefonoColegio
        {
            get
            {
                return this.telefonoColegio;
            }
            set
            {
                this.telefonoColegio = value;
            }
        }



        public string _Region
        {
            get { return region; }
            set { region = value; }
        }
    }
}

