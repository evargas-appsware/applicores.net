﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class RespuestaConsultarRBDClave
    {
        public string CLAVE { get; set; }

        public string MENSAJE { get; set; }

        public string RBD { get; set; }
    }
}

