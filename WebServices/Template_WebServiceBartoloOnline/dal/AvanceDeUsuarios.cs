﻿namespace DAL
{
    using Microsoft.Practices.EnterpriseLibrary.Data;
    using System;
    using System.Data;
    using System.Data.Common;

    public class AvanceDeUsuarios
    {
        private int camino;
        private int caminoCapituloSuperado = 0;
        private int capitulo;
        private string curso;
        private int fechaCreacion;
        private int idActividad;
        private string idRegistro;
        private int idUsuario;
        private string nombre;
        private int puntos;
        private int ruta;
        private int superado;
        private string timeStamp;
        private int ultimoAcceso;
        private int usuarioCaminoId;

        public int habilitacionUsuarios(int _valor)
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_USUARIO_HABILITACION");
                _db.AddInParameter(_command, "@ID_USUARIO", DbType.Int32, this._IdUsuario);
                _db.AddInParameter(_command, "@VALOR", DbType.Int32, _valor);
                return Convert.ToInt32(_db.ExecuteDataSet(_command).Tables[0].Rows[0][0]);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public DataSet HistoricoActividadesAvance()
        {
            Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
            DbCommand _command = _db.GetStoredProcCommand("USUARIOS_ACTIVIDADES_AVANCE_HISTORICO");
            _db.AddInParameter(_command, "@ID_ACTIVACION", DbType.String, this._IdRegistro);
            _db.AddInParameter(_command, "@TIMESTAMP", DbType.String, this._TimeStamp);
            return _db.ExecuteDataSet(_command);
        }

        public DataSet HistoricoPuntajeUsuarios()
        {
            Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
            DbCommand _command = _db.GetStoredProcCommand("SINCRONIZAR_PUNTOS_AVANCE");
            _db.AddInParameter(_command, "@ID_ACTIVACION", DbType.String, this._IdRegistro);
            _db.AddInParameter(_command, "@TIMESTAMP", DbType.String, this._TimeStamp);
            return _db.ExecuteDataSet(_command);
        }

        public DataSet HistoricoPuntajeUsuariosCapitulos()
        {
            Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
            DbCommand _command = _db.GetStoredProcCommand("SINCRONIZAR_PUNTOS_CAPITULOS");
            _db.AddInParameter(_command, "@ID_ACTIVACION", DbType.String, this._IdRegistro);
            _db.AddInParameter(_command, "@TIMESTAMP", DbType.String, this._TimeStamp);
            return _db.ExecuteDataSet(_command);
        }

        public DataSet ListaUsuariosDeshabilitados()
        {
            Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
            DbCommand _command = _db.GetStoredProcCommand("USUARIOS_DESHABILITADOS");
            _db.AddInParameter(_command, "@ID_ACTIVACION", DbType.String, this._IdRegistro);
            _db.AddInParameter(_command, "@TIMESTAMP", DbType.String, this._TimeStamp);
            return _db.ExecuteDataSet(_command);
        }

        public DataSet UsuariosActividadesAvance()
        {
            Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
            DbCommand _command = _db.GetStoredProcCommand("USUARIOS_ACTIVIDADES_AVANCE");
            _db.AddInParameter(_command, "@ID_ACTIVACION", DbType.String, this._IdRegistro);
            _db.AddInParameter(_command, "@CURSO", DbType.String, this._Curso);
            _db.AddInParameter(_command, "@NOMBRE", DbType.String, this._Nombre);
            _db.AddInParameter(_command, "@ANIO", DbType.Int32, this._FechaCreacion);
            return _db.ExecuteDataSet(_command);
        }

        public int UsuariosActividadesUPD()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_USUARIOS_ACTIVIDADES_UPD");
                _db.AddInParameter(_command, "@PUNTOS", DbType.Int32, this._Puntos);
                _db.AddInParameter(_command, "@SUPERADO", DbType.Int32, this._Superado);
                _db.AddInParameter(_command, "@ULTIMO_ACCESO", DbType.Int32, this._UltimoAcceso);
                _db.AddInParameter(_command, "@ACTIVIDAD", DbType.Int32, this._IdActividad);
                _db.AddInParameter(_command, "@RUTA", DbType.Int32, this._Ruta);
                _db.AddInParameter(_command, "@ID_USUARIO", DbType.Int32, this._IdUsuario);
                _db.AddInParameter(_command, "@ID_USUARIO_CAMINO", DbType.Int32, this._UsuarioCaminoId);
                return Convert.ToInt32(_db.ExecuteDataSet(_command).Tables[0].Rows[0][0]);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public DataSet UsuariosAvanceMasivo()
        {
            Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
            DbCommand _command = _db.GetStoredProcCommand("USUARIOS_AVANCE_MASIVO");
            _db.AddInParameter(_command, "@ID_ACTIVACION", DbType.String, this._IdRegistro);
            _db.AddInParameter(_command, "@TIMESTAMP", DbType.String, this._TimeStamp);
            return _db.ExecuteDataSet(_command);
        }

        public DataSet UsuariosAvanceSingle()
        {
            Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
            DbCommand _command = _db.GetStoredProcCommand("USUARIOS_AVANCE_SINGLE");
            _db.AddInParameter(_command, "@ID_ACTIVACION", DbType.String, this._IdRegistro);
            _db.AddInParameter(_command, "@CURSO", DbType.String, this._Curso);
            _db.AddInParameter(_command, "@NOMBRE", DbType.String, this._Nombre);
            _db.AddInParameter(_command, "@ANIO", DbType.Int32, this._FechaCreacion);
            return _db.ExecuteDataSet(_command);
        }

        public int UsuariosCapitulosUPD()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_USUARIO_CAPITULO_UPD");
                _db.AddInParameter(_command, "@CAPITULO", DbType.Int32, this._Capitulo);
                _db.AddInParameter(_command, "@CAMINO", DbType.Int32, this._Camino);
                _db.AddInParameter(_command, "@ID_USUARIO", DbType.Int32, this._IdUsuario);
                _db.AddInParameter(_command, "@TERMINADO", DbType.Int32, this._CaminoCapituloSuperado);
                return Convert.ToInt32(_db.ExecuteDataSet(_command).Tables[0].Rows[0][0]);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int UsuarioUPD()
        {
            try
            {
                Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
                DbCommand _command = _db.GetStoredProcCommand("BTO_USUARIO_UPD");
                _db.AddInParameter(_command, "@ID_ACTIVACION", DbType.String, this._IdRegistro);
                _db.AddInParameter(_command, "@CURSO", DbType.String, this._Curso);
                _db.AddInParameter(_command, "@NOMBRE", DbType.String, this._Nombre);
                _db.AddInParameter(_command, "@ANIO", DbType.String, this._FechaCreacion);
                return Convert.ToInt32(_db.ExecuteDataSet(_command).Tables[0].Rows[0][0]);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public DataSet verificarHabilitado()
        {
            Database _db = DatabaseFactory.CreateDatabase("ConexionDB");
            DbCommand _command = _db.GetStoredProcCommand("BTO_USUARIO_VERIFICAR_HABILITADO");
            _db.AddInParameter(_command, "@ID_ACTIVACION", DbType.String, this._IdRegistro);
            _db.AddInParameter(_command, "@CURSO", DbType.String, this._Curso);
            _db.AddInParameter(_command, "@NOMBRE", DbType.String, this._Nombre);
            _db.AddInParameter(_command, "@ANIO", DbType.Int32, this._FechaCreacion);
            return _db.ExecuteDataSet(_command);
        }

        public int _Camino
        {
            get
            {
                return this.camino;
            }
            set
            {
                this.camino = value;
            }
        }

        public int _CaminoCapituloSuperado
        {
            get
            {
                return this.caminoCapituloSuperado;
            }
            set
            {
                this.caminoCapituloSuperado = value;
            }
        }

        public int _Capitulo
        {
            get
            {
                return this.capitulo;
            }
            set
            {
                this.capitulo = value;
            }
        }

        public string _Curso
        {
            get
            {
                return this.curso;
            }
            set
            {
                this.curso = value;
            }
        }

        public int _FechaCreacion
        {
            get
            {
                return this.fechaCreacion;
            }
            set
            {
                this.fechaCreacion = value;
            }
        }

        public int _IdActividad
        {
            get
            {
                return this.idActividad;
            }
            set
            {
                this.idActividad = value;
            }
        }

        public string _IdRegistro
        {
            get
            {
                return this.idRegistro;
            }
            set
            {
                this.idRegistro = value;
            }
        }

        public int _IdUsuario
        {
            get
            {
                return this.idUsuario;
            }
            set
            {
                this.idUsuario = value;
            }
        }

        public string _Nombre
        {
            get
            {
                return this.nombre;
            }
            set
            {
                this.nombre = value;
            }
        }

        public int _Puntos
        {
            get
            {
                return this.puntos;
            }
            set
            {
                this.puntos = value;
            }
        }

        public int _Ruta
        {
            get
            {
                return this.ruta;
            }
            set
            {
                this.ruta = value;
            }
        }

        public int _Superado
        {
            get
            {
                return this.superado;
            }
            set
            {
                this.superado = value;
            }
        }

        public string _TimeStamp
        {
            get
            {
                return this.timeStamp;
            }
            set
            {
                this.timeStamp = value;
            }
        }

        public int _UltimoAcceso
        {
            get
            {
                return this.ultimoAcceso;
            }
            set
            {
                this.ultimoAcceso = value;
            }
        }

        public int _UsuarioCaminoId
        {
            get
            {
                return this.usuarioCaminoId;
            }
            set
            {
                this.usuarioCaminoId = value;
            }
        }
    }
}

