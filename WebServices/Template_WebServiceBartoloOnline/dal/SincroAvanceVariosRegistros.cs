﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class SincroAvanceVariosRegistros
    {
        public string ID_REGISTRO { get; set; }

        public DatosSincroAvanceUsuarios[] REGISTROS { get; set; }

        public string TIMESTAMP { get; set; }
    }
}

