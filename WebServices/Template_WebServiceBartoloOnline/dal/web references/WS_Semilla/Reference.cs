﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.261
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.261.
// 
#pragma warning disable 1591

namespace DAL.WS_Semilla {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.ComponentModel;
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="SemillaServiciosSoap", Namespace="SemillaServicios")]
    public partial class SemillaServiciosService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback getSemillaServiciosOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public SemillaServiciosService() {
            this.Url = global::DAL.Properties.Settings.Default.DAL_WS_Semilla_SemillaServiciosService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event getSemillaServiciosCompletedEventHandler getSemillaServiciosCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("getSemillaServicios", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("SemillaServicios", Namespace="http://wwwfs.mineduc.cl/Archivos/Schemas/")]
        public SemillaServicios getSemillaServicios([System.Xml.Serialization.XmlElementAttribute(Namespace="http://wwwfs.mineduc.cl/Archivos/Schemas/")] EntradaSemillaServicios EntradaSemillaServicios) {
            object[] results = this.Invoke("getSemillaServicios", new object[] {
                        EntradaSemillaServicios});
            return ((SemillaServicios)(results[0]));
        }
        
        /// <remarks/>
        public void getSemillaServiciosAsync(EntradaSemillaServicios EntradaSemillaServicios) {
            this.getSemillaServiciosAsync(EntradaSemillaServicios, null);
        }
        
        /// <remarks/>
        public void getSemillaServiciosAsync(EntradaSemillaServicios EntradaSemillaServicios, object userState) {
            if ((this.getSemillaServiciosOperationCompleted == null)) {
                this.getSemillaServiciosOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetSemillaServiciosOperationCompleted);
            }
            this.InvokeAsync("getSemillaServicios", new object[] {
                        EntradaSemillaServicios}, this.getSemillaServiciosOperationCompleted, userState);
        }
        
        private void OngetSemillaServiciosOperationCompleted(object arg) {
            if ((this.getSemillaServiciosCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.getSemillaServiciosCompleted(this, new getSemillaServiciosCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://wwwfs.mineduc.cl/Archivos/Schemas/")]
    public partial class EntradaSemillaServicios {
        
        private string clienteIdField;
        
        private string convenioIdField;
        
        private string convenioTokenField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
        public string ClienteId {
            get {
                return this.clienteIdField;
            }
            set {
                this.clienteIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer")]
        public string ConvenioId {
            get {
                return this.convenioIdField;
            }
            set {
                this.convenioIdField = value;
            }
        }
        
        /// <remarks/>
        public string ConvenioToken {
            get {
                return this.convenioTokenField;
            }
            set {
                this.convenioTokenField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://wwwfs.mineduc.cl/Archivos/Schemas/")]
    public partial class SemillaServicios {
        
        private string valorSemillaField;
        
        /// <remarks/>
        public string ValorSemilla {
            get {
                return this.valorSemillaField;
            }
            set {
                this.valorSemillaField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void getSemillaServiciosCompletedEventHandler(object sender, getSemillaServiciosCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class getSemillaServiciosCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal getSemillaServiciosCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public SemillaServicios Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((SemillaServicios)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591