﻿namespace DAL
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class PuntajeDeUsuarios
    {
        public string CURSO { get; set; }

        public int FECHA { get; set; }

        public int HABILITADO { get; set; }

        public string NOMBRE { get; set; }

        public int PUNTOS { get; set; }
    }
}

